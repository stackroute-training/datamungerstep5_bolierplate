﻿namespace DbEngine.Query
{
    //This class represents a single data row. It should contain a string array named as RowValues to hold all the values in a row. Use constructor to initialize the property.
    public class Row
    {
        public string[] RowValues { get; set; }

        public Row(string id, string season, string city, string date, string team1, string team2, string tossWinner, string tDesci, string result, string dl_applied, string winner, string winByRuns, string winByWick, string pom, string venue, string u1, string u2, string u3)
        {
            
            RowValues = new[] { id, season, city, date, team1, team2, tossWinner, tDesci, result, dl_applied, winner, winByRuns, winByWick, pom, venue, u1, u2, u3 };
        }

    }
}
