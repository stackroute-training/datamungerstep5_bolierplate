using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace DbEngine.Query.Parser
{
    public class QueryParser
    {
        private QueryParameter queryParameter;
        public QueryParser()
        {
            queryParameter = new QueryParameter();
        }

        /*
     this method will parse the queryString and will return the object of
     QueryParameter class
     */
        public QueryParameter ParseQuery(string queryString)
        {
            /*
              extract the name of the file from the query. File name can be found after the "from" clause.
             */
            queryParameter.File = GetFileName(queryString);

            /*
             extract the order by fields from the query string. Please note that we will need to extract the field(s) after "order by" clause in the query, if at all the order by clause exists. For eg: select city,winner,team1,team2 from data/ipl.csv order by city from the query mentioned above, we need to extract "city". Please note that we can have more than one order by fields.
             */
            queryParameter.OrderByFields = GetOrderByFields(queryString);

            /*
             extract the group by fields from the query string. Please note that we will need to extract the field(s) after "group by" clause in the query, if at all the group by clause exists. For eg: select city,max(win_by_runs) from data/ipl.csv group by city from the query mentioned above, we need to extract "city". Please note that we can have more than one group by fields.
             */
            queryParameter.GroupByFields = GetGroupByFields(queryString);

            /*
             * extract the selected fields from the query string. Please note that we will need to extract the field(s) after "select" clause followed by a space from the query string. For eg: select city,win_by_runs from data/ipl.csv from the query mentioned above, we need to extract "city" and "win_by_runs". Please note that we might have a field containing name "from_date" or "from_hrs". Hence, consider this while parsing.
             */
            queryParameter.Fields = GetFields(queryString);



            /*
             extract the conditions from the query string(if exists). for each condition, we need to capture the following: 
              1. Name of field 
              2. condition 
              3. value
            For eg: select city,winner,team1,team2,player_of_match from data/ipl.csv where season >= 2008 or toss_decision != bat
             
             here, for the first condition, "season>=2008" we need to capture: 
              1. Name of field: season 
              2. condition: >= 
              3. value: 2008
              
              the query might contain multiple conditions separated by OR/AND operators.
              Please consider this while parsing the conditions.
              
             */
            queryParameter.Restrictions = GetRestrictions(queryString);

            /*
              extract the logical operators(AND/OR) from the query, if at all it is present. For eg: select city,winner,team1,team2, player_of_match from  data/ipl.csv where season >= 2008 or toss_decision != bat and city = bangalore
             
              the query mentioned above in the example should return a List of Strings containing [or,and]
             */
            queryParameter.LogicalOperators = GetLogicalOperators(queryString);

            /*
             extract the aggregate functions from the query. The presence of the aggregate functions can determined if we have either "min" or "max" or "sum" or "count" or "avg" followed by opening braces"(" after "select" clause in the query string. in case it is present, then we will have to extract the same. For
              each aggregate functions, we need to know the following: 
             1. type of aggregate function(min/max/count/sum/avg) 
             2. field on which the aggregate function is being applied
              
              Please note that more than one aggregate function can be present in a query
              
             */
            queryParameter.AggregateFunctions = GetAggregateFunctions(queryString);

            

            return queryParameter;
        }
        private string GetFileName(string query)
        {
            string str = "";
            string str2 = "";
            for (int i = 0; i < query.Length; i++)
            {
                if (query[i] == '.')
                {
                    for (int j = i - 1; j >= 0; j--)
                    {
                        if (query[j] == ' ')
                        {
                            break;
                        }
                        else
                        {
                            str += query[j];
                        }
                    }

                    for (int l = str.Length - 1; l >= 0; l--)
                    {
                        str2 += str[l];
                    }

                    str2 += '.';
                    for (int k = i + 1; k < query.Length; k++)
                    {
                        if (query[k] == ' ')
                        {
                            break;
                        }
                        else
                        {
                            str2 += query[k];
                        }
                    }
                }
            }
            return str2;
        }

        private string GetBaseQuery(string query)
        {
            string[] str = query.Split();
            string str2 = "";
            foreach (string str3 in str)
            {
                if (str3 == "where")
                {
                    break;
                }
                else
                {
                    str2 += str3 + " ";
                }
            }
            return str2.Trim();
        }

        private string GetConditionsPartQuery(string queryString)
        {
            string[] str = queryString.Split();
            string str2 = "";
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] != "where")
                {
                    str2 += str[i] + " ";
                }
                else
                {
                    if (str[i] == "where")
                    {
                        str2 += str[i];
                    }
                    else
                    {
                        break;
                    }
                    break;
                }
            }
            queryString = queryString.Replace(str2, "");
            return queryString.Trim();
        }

        private string[] GetConditions(string queryString)
        {
            string query = GetConditionsPartQuery(queryString);
            string[] str3 = query.Split();
            string[] str = new string[3];
            string s = "";
            if (query.Contains(" and ") && query.Contains(" or "))
            {
                int indx1 = Array.IndexOf(str3, "and");
                int indx2 = Array.IndexOf(str3, "or");
                if (indx1 < indx2)
                {
                    for (int i = 0; i < indx1; i++)
                    {
                        s += str3[i] + " ";
                    }
                    s += ',';
                    for (int j = indx1 + 1; j < indx2; j++)
                    {
                        s += str3[j] + " ";
                    }
                    s += ',';
                    for (int k = indx2 + 1; k < str3.Length; k++)
                    {
                        s += str3[k] + " ";
                    }
                }
                else
                {
                    for (int i = 0; i < indx2; i++)
                    {
                        s += str3[i] + " ";
                    }
                    s += ',';
                    for (int j = indx2 + 1; j < indx1; j++)
                    {
                        s += str3[j] + " ";
                    }
                    s += ',';
                    for (int k = indx1 + 1; k < str3.Length; k++)
                    {
                        s += str3[k] + " ";
                    }
                }
                str = s.Split(',');
            }
            else if (query.Contains("and"))
            {
                str = query.Split(" and ");
            }
            else if (query.Contains("or"))
            {
                str = query.Split(" or ");
            }
            else
            {
                str[0] = query;
            }

            return str;
        }

        private List<string> GetFields(string queryString)
        {
            string[] str = queryString.Split();
            string str2 = "";
            foreach (string s in str)
            {
                if (s == "select")
                {
                    continue;
                }
                else if (s == "from")
                {
                    break;
                }
                else
                {
                    str2 += s;
                }
            }
            string[] str3 = str2.Split(',');
            List<string> str4 = new List<string>();
            for (int i = 0; i < str2.Length; i++)
            {
                str4[i] = str3[i];
            }
            return str4;
        }

        private List<Restriction> GetRestrictions(string query)
        {

            string[] str = GetConditions(query);
            string[] ss = new string[3];
            string field = "";
            string val = "";
            string condition = "";
            List<Restriction> filters = new List<Restriction>();
            for(int i = 0; i< str.Length; i++)
            {
                if (str[i] != null)
                {
                    string[] s = str[i].Split();
                    field = s[0];
                    val = s[2];
                    condition = s[1];
                    Restriction filterCondition = new Restriction(field, val, condition);
                    filters.Add(filterCondition);
                }
                else
                {
                    Console.WriteLine("empty");
                }
            }
            
            return filters;
        }

        private List<string> GetLogicalOperators(string queryString)
        {
            string[] str = queryString.Split();

            if (queryString.Contains(" and ") && queryString.Contains(" or "))
            {
                List<string> ans = new List<string>();
                int j = 0;
                for (int i = 0; i < str.Length; i++)
                {
                    if (str[i] == "and" || str[i] == "or")
                    {
                        ans[j] = str[i];
                        j++;
                    }
                }
                return ans;
            }
            else if (queryString.Contains("and"))
            {
                for (int i = 0; i < str.Length; i++)
                {
                    if (str[i] == "and")
                    {
                        return new List<string> { str[i] };
                    }
                }
            }
            else if (queryString.Contains(" or "))
            {
                for (int i = 0; i < str.Length; i++)
                {
                    if (str[i] == "or")
                    {
                        return new List<string> { str[i] };
                    }
                }
            }

            return null;
        }

        private List<AggregateFunction> GetAggregateFunctions(string queryString)
        {
            string field = "";
            string funct = "";
            if (queryString.Contains("count(") || queryString.Contains("sum(") || queryString.Contains("avg(") || queryString.Contains("min(") || queryString.Contains("max("))
            {
                string[] str = queryString.Split();
                if (str[1].Contains(","))
                {
                    string[] str1 = str[1].Split(',');
                   List<AggregateFunction> aggregates = new List<AggregateFunction>();
                    for (int i = 0; i < str1.Length; i++)
                    {
                        int indx = str1[i].IndexOf('(');
                        int indx2 = str1[i].IndexOf(')');
                        field = str1[i].Substring(indx + 1, indx2 - indx - 1);
                        funct = str1[i].Substring(0, indx);
                        aggregates[i] = new AggregateFunction(field, funct);
                    }
                    return aggregates;
                }
                else
                {
                    int indx = str[1].IndexOf('(');
                    int indx2 = str[1].IndexOf(')');
                    field = str[1].Substring(indx + 1, indx2 - indx - 1);
                    funct = str[1].Substring(0, indx);
                    AggregateFunction aggregateFunction = new AggregateFunction(field, funct);
                    List<AggregateFunction> aggregates = new List<AggregateFunction>() { aggregateFunction };
                    return aggregates;
                }

            }
            return null;
        }

        private List<string> GetOrderByFields(string queryString)
        {
            if (queryString.Contains("group by") && queryString.Contains("order by"))
            {
                int indx1 = queryString.IndexOf("order by");
                int indx2 = queryString.IndexOf("group by");
                string str = queryString.Substring(indx1 + 9);
                return new List<string> { str };
            }
            else if (queryString.Contains("order by"))
            {
                int indx = queryString.IndexOf("by");
                string str = queryString.Substring(indx + 3);
                string[] str2 = str.Split();
                List<string> str3 = new List<string>();
                for(int i = 0; i < str2.Length; i++)
                {
                    str3[i] = str2[i];
                }
                
                return str3;
            }
            else
            {
                return null;
            }
        }

        private List<string> GetGroupByFields(string queryString)
        {
            if (queryString.Contains("group by") && queryString.Contains("order by"))
            {
                int indx1 = queryString.IndexOf("order by");
                int indx2 = queryString.IndexOf("group by");
                string str = queryString.Substring(indx2 + 9, 6);

                return new List<string> { str };
            }
            else if (queryString.Contains("group"))
            {
                int indx = queryString.IndexOf("by");
                string str = queryString.Substring(indx + 3);
                string[] str2 = str.Split();
                List<string> str3 = new List<string>();
                for (int i = 0; i < str2.Length; i++)
                {
                    str3[i] = str2[i];
                }
                return str3;
            }
            else
            {
                return null;
            }
        }
    }
}