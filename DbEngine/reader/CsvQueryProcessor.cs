using System;
using System.IO;
using System.Globalization;
using System.Text.RegularExpressions;
using DbEngine.Query;
using DbEngine.Query.Parser;
using System.Collections.Generic;

namespace DbEngine.Reader
{
    public class CsvQueryProcessor : QueryProcessingEngine
    {
        /*
	    parameterized constructor to initialize filename. As you are trying to perform file reading, hence you need to be ready to handle the IO Exceptions.
	   */
        private readonly string _fileName;
        public CsvQueryProcessor(string fileName)
        {
            this._fileName = fileName;
        }

        /*
          read the first line which contains the header. Please note that the headers can contain spaces in between them. For eg: city, winner
          */
        public override Header GetHeader()
        {
            try
            {
                string line;
                using (StreamReader reader = new StreamReader(_fileName))
                {

                    line = reader.ReadLine();
                    reader.Close();
                    string[] parts = line.Split(',');
                    if (parts.Length == 18)
                    {
                        Header header = new Header(parts[0], parts[1], parts[2], parts[3], parts[4], parts[5], parts[6], parts[7], parts[8], parts[9], parts[10], parts[11], parts[12], parts[13], parts[14], parts[15], parts[16], parts[17]);
                        return header;
                    }

                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        /*
	     implementation of getColumnType() method. To find out the data types, we will
	     read the first line after the header row from the file and extract the field values from it. In
	     the previous assignment, we have tried to convert a specific field value to
	     Integer or Double. However, in this assignment, we are going to use Regular
	     Expression to find the appropriate data type of a field. Integers: should
	     contain only digits without decimal point Double: should contain digits as
	     well as decimal point Date: Dates can be written in many formats in the CSV
	     file. However, in this assignment,we will test for the following date
	     formats('dd/mm/yyyy','mm/dd/yyyy','dd-mon-yy','dd-mon-yyyy','dd-month-yy','dd-month-yyyy','yyyy-mm-dd')
	    */
        public override DataTypeDefinitions GetColumnType()
        {
            try
            {


                using (StreamReader reader = new StreamReader(_fileName))
                {
                    List<string[]> line1 = new List<string[]>();

                    while (!reader.EndOfStream)
                    {
                        var file = reader.ReadLine();
                        var val = file.Split(',');
                        line1.Add(val);
                    }

                    reader.Close();
                    string[] parts = line1[1];
                    string[] parts2 = new string[parts.Length];
                    Regex digitCheck = new Regex(@"\d");
                    Regex alphaCheck = new Regex(@"\D");
                    Regex d1 = new Regex(@"^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d$"); // mm dd yyyy (/-.)
                    Regex d2 = new Regex(@"^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"); // dd mm yyyy
                    Regex d3 = new Regex(@"^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])"); // yyyy mm dd

                    for (int i = 0; i < parts.Length; i++)
                    {
                        Match match1 = digitCheck.Match(parts[i]);
                        Match match2 = alphaCheck.Match(parts[i]);
                        Match match3 = d1.Match(parts[i]);
                        Match match4 = d2.Match(parts[i]);
                        Match match5 = d3.Match(parts[i]);
                        if (match1.Success)
                        {
                            if (Int32.TryParse(parts[i], out int val))
                            {
                                Type t = val.GetType();
                                parts2[i] = t.ToString();
                            }
                            else if (match3.Success || match4.Success || match5.Success)
                            {
                                parts2[i] = typeof(DateTime).ToString();
                            }
                        }
                        else if (match2.Success)
                        {
                            Type t = parts[i].GetType();
                            parts2[i] = t.ToString();

                        }
                        else if (parts[i] == "")
                        {
                            parts2[i] = typeof(object).ToString();

                        }

                    }

                    DataTypeDefinitions data = new DataTypeDefinitions(parts2[0], parts2[1], parts2[2], parts2[3], parts2[4], parts2[5], parts2[6], parts2[7], parts2[8], parts2[9], parts2[10], parts2[11], parts2[12], parts2[13], parts2[14], parts2[15], parts2[16], parts2[17]);
                    return data;


                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        /*
	    This method will take QueryParameter object as a parameter which contains the parsed query and will process and populate the DataSet
	    */
        public override DataSet GetDataRow(QueryParameter queryParameter)
        {
            /*
                    * from QueryParameter object, read one condition at a time and evaluate the
                    * same. For evaluating the conditions, we will use evaluateExpressions() method
                    * of Filter class. Please note that evaluation of expression will be done
                    * differently based on the data type of the field. In case the query is having
                    * multiple conditions, you need to evaluate the overall expression i.e. if we
                    * have OR operator between two conditions, then the row will be selected if any
                    * of the condition is satisfied. However, in case of AND operator, the row will
                    * be selected only if both of them are satisfied.
                    */
            bool headingRemoved = false;
            List<string[]> line1 = new List<string[]>();
            List<DataSet> dataSet = new List<DataSet>();
            using (StreamReader reader = new StreamReader(_fileName))
            {
                
                while (!reader.EndOfStream)
                {
                    var file = reader.ReadLine();
                    var val = file.Split(',');
                    line1.Add(val);
                }
                reader.Close();
            }
            headingRemoved = line1.Remove(line1[0]);
            if(queryParameter.Fields.Count == 0 && queryParameter.LogicalOperators.Count == 0)
            {
                for(int i = 0; i < line1.Count; i++)
                {
                    Row row = new Row(line1[i]);
                    dataSet.Add(row);
                }
            }

            return null;
            
        }
    }
}
